# Overview
This is a simple Program for calculating optimal resistor values from a certain E-Series for a voltage divider.

## Usage
When launched, the program will prompt for the E-Series to use, then the input and output voltage, where the input voltage is the voltage across both resistors and the output voltage it the voltage across R1. It will then provide the best Resistor combination, where the smaller resistor is between 1k and 10k. If you want to scale the Resistor values, you can multiply both by any power of 10.

## Compilation
This program uses CMake and a Makefile. The Makefile is mainly used to make my own workflow easier.

### Compiling using CMake  
Create a new build directory and invoke cmake from it, otherwise my own Makefile will be overwritten.
```bash
mkdir build && cd build
cmake ..
make
```

### Compiling using Makefile
All the Makefile does is invoke cmake in the correct directories for a debug and a relase build. If you have not built the Software before, the target setup is needed:
```bash
make setup
```
This will create a debug and a release directory and compile both versions. After that, a specific version can be built with `make debug` or `make release`. The default target will build both.

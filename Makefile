all: debug release

setup:
	rm -rf debug
	rm -rf release
	cmake . -B debug -DCMAKE_BUILD_TYPE=DEBUG -G Ninja
	cmake . -B release -DCMAKE_BUILD_TYPE=RELEASE -G Ninja

.PHONY:debug

.PHONY: release

debug:
	cd debug && ninja

release:
	cd release && ninja

clean:
	cd debug && ninja clean
	cd release && ninja clean


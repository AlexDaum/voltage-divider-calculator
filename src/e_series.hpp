#pragma once

class e_series_gen;

class e_series_iterator {
	friend class e_series_gen;

  public:
	double operator*() const;
	void operator++();
	bool operator!=(const e_series_iterator &other) const;

  private:
	e_series_iterator(const e_series_gen &generator, int startIdx) noexcept
		: idx(startIdx), generator(generator) {}

	int idx;
	const e_series_gen &generator;
};

class e_series_gen {
  public:
	using iterator = e_series_iterator;
	explicit e_series_gen(int e_series);
	double getAt(int i) const noexcept;
	int getElementCount() const noexcept { return e_series; }

	iterator begin() const noexcept;
	iterator end() const noexcept;

  private:
	const int skip;
	const int e_series;
};

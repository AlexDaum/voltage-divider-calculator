#include "e_series.hpp"
#include <array>
#include <iostream>
#include <tuple>

int main() {
	int e_series;
	std::cout << "Enter Number of E Series to use (e.g. 12 for E12). Supported "
				 "values are 3, 6, 12, 24, 48, 96"
			  << std::endl;
	std::cin >> e_series;
	if(96 % e_series != 0) {
		std::cout << "Invalid E-Series" << std::endl;
		return -1;
	}
	auto gen = e_series_gen(e_series);

	double Vin, Vout;
	std::cout << "Enter input Voltage: ";
	std::cin >> Vin;
	std::cout << "Enter output Voltage: ";
	std::cin >> Vout;
	std::cout << std::endl;

	double ratio = Vout / Vin;

	std::tuple<double, double> bestFit;
	double bestFitError = 10000;

	for (double r1 : gen) {
		// First find correct decade multiplier for r2
		double decade = 1;
		for (;;) {
			double actualRatio = r1 / (r1 + 10 * decade);
			if (actualRatio > ratio)
				decade *= 10;
			else
				break;
		}
		for (double r2 : gen) {
			r2 = decade * r2;
			double actualRatio = r1 / (r1 + r2);
			double error = std::abs((actualRatio - ratio) / ratio);
			if (error < bestFitError) {
				bestFitError = error;
				bestFit = {r1, r2};
			}
			// also try with r1 one decade higher
			double r10 = 10 * r1;
			actualRatio = r10 / (r10 + r2);
			error = std::abs((actualRatio - ratio) / ratio);
			if (error < bestFitError) {
				bestFitError = error;
				bestFit = {r10, r2};
			}
		}
	}

	double bestR1 = std::get<0>(bestFit);
	double bestR2 = std::get<1>(bestFit);

	std::cout << "Best Fit with resistor R1=" << bestR1;
	std::cout << "k, R2=" << bestR2 << "k" << std::endl;
	std::cout << "Actual Output Voltage on R1: "
			  << (Vin * (bestR1 / (bestR1 + bestR2))) << "V" << std::endl;
	std::cout << "Error is " << bestFitError * 100 << "%" << std::endl;
	return 0; }
